# Getting Started (Gradle)


## Scaffold

    spring init --name=Redis1 -g=spring5 -a=redis1 -d=webflux,data-redis-reactive,devtools,actuator --boot=2.0.0.BUILD-SNAPSHOT --build=gradle  spring5-redis1
    spring init --name=Redis1Client -g=spring5 -a=redis1.client -d=spring-shell,webflux,data-redis-reactive,devtools --boot=2.0.0.BUILD-SNAPSHOT --build=gradle  spring5-redis1-client



## Build

    ./gradlew clean build



## Run

    ./gradlew bootRun








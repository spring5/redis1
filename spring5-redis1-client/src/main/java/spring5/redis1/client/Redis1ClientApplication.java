package spring5.redis1.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Redis1ClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(Redis1ClientApplication.class, args);
	}
}
